<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->date('checkin');
            $table->date('checkout');
            $table->integer('nights');
            $table->integer('pax');
            $table->decimal('costs', 8, 2)->nullable(true);
            $table->decimal('discounts', 8, 2)->nullable(true);
            $table->decimal('cleanning', 8, 2)->nullable(true);
            $table->decimal('comissions', 8, 2)->nullable(true);
            $table->decimal('receivable', 8, 2)->nullable(true);
            $table->boolean('recieved')->nullable(true);
            $table->binary('documentation')->nullable(true);
            $table->unsignedInteger('platform_id');


            $table->foreign('platform_id')->references('id')->on('platforms');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
