
@extends('layouts.app')

@section('content')



<!-- Advanced Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Create customer</h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            {{-- <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li> --}}
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <form method="POST" action="{{route('customer.update', $customer)}}" class="col s12" enctype="multipart/form-data"  >
                    @method('PUT')
                    @csrf
                    <div class="col-xs-6">
                        <select name="platform" class="form-control show-tick">
                            <option value={{$customer->platform->id}}>{{$customer->platform->name}}</option>
                            @foreach ($platforms as $platform)
                            <option value="{{$platform->id}}">{{$platform->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" value="{{$customer->name}}" class="form-control" name="guest_name" maxlength="50" minlength="3" required>
                                <label class="form-label">Guest Name</label>
                            </div>
                            <div class="help-info">Please provide with Guest name</div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="input-daterange input-group" id="bs_datepicker_range_container">
                            <div class="form-line">
                                <input value="{{$customer->checkin}}" name="checkin" type="text" class="form-control" placeholder="Date start...">
                            </div>
                            <span class="input-group-addon">to</span>
                            <div class="form-line">
                                <input value="{{$customer->checkout}}" name="checkout" type="text" class="form-control" placeholder="Date end...">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input value="{{$customer->nights}}" name="nights" type="text" class="form-control" required>
                                <label class="form-label">Nights</label>
                            </div>
                            <div class="help-info">Min. 3, Max. 10 characters</div>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input value="{{$customer->pax}}" name="PAX" type="number" class="form-control" required>
                                <label class="form-label">PAX</label>
                            </div>
                            <div class="help-info">How many guests</div>
                        </div>
                    </div>
                    <div class="col-xs-6">

                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input value="{{$customer->costs}}" name="pernight" type="text" class="form-control money-dollar" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Per Night</div>
                        </div>
                    </div>
                    <div class="col-xs-6">

                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input value="{{$customer->cleanning}}" name="cleanning" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Cleanning</div>
                        </div>
                    </div>
                    <div class="col-xs-6">

                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input value="{{$customer->discounts}}" name="discount" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Discount</div>
                        </div>
                    </div>
                    <div class="col-xs-6">

                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input value="{{$customer->comissions}}" name="comissions" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Comissions</div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="demo-switch">
                            <div class="switch">
                                        <label>Waiting for payment <input name="recieved" value="1" {{ $customer->recieved ? 'checked="checked"' : ''}} type="checkbox" ><span class="lever"></span>Payed</label>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-xs-6">
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input value="{{$customer->documentation}}" name="comissions" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Comissions</div>
                        </div>
                    </div> --}}


                    {{-- <div class="col-xs-6"> --}}
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input value="{{$customer->receivable}}" name="receivable" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">receivable</div>
                        </div>
                        {{-- </div> --}}
                        <!-- File Upload | Drag & Drop OR With Click & Choose -->
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            FILE UPLOAD - DRAG & DROP OR WITH CLICK & CHOOSE
                                            <small>Taken from <a href="http://www.dropzonejs.com/" target="_blank">www.dropzonejs.com</a></small>
                                        </h2>
                                        <ul class="header-dropdown m-r--5">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="javascript:void(0);">Action</a></li>
                                                    <li><a href="javascript:void(0);">Another action</a></li>
                                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="body">
                                        {{-- <form action="/" id="frmFileUpload" class="dropzone" method="post" enctype="multipart/form-data"> --}}
                                            <div class="dz-message">
                                                <div class="drag-icon-cph">
                                                    <i class="material-icons">touch_app</i>
                                                </div>
                                                <h3>Drop files here or click to upload.</h3>
                                                <em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em>
                                            </div>
                                            <div class="fallback">
                                                <input name="documentation" type="file" multiple />
                                            </div>
                                            {{-- </form> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- #END# File Upload | Drag & Drop OR With Click & Choose -->

                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        @endsection
