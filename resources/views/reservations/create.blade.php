
@extends('layouts.app')

@section('content')



<!-- Advanced Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>New Reservation</h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <form method="POST" action="{{route('customer.store')}}" class="col s12">
                    @method('POST')
                    @csrf
                    <div class="col-xs-6">
                        <select name="platform" class="form-control show-tick">
                            <option value="">-- Please select --</option>
                            @foreach ($platforms as $platform)
                            <option value="{{$platform->id}}">{{$platform->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="guest_name" maxlength="50" minlength="3" required>
                                <label class="form-label">Guest Name</label>
                            </div>
                            <div class="help-info">Please provide with Guest name</div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        {{-- <h2 class="card-inside-title">Range</h2> --}}
                        <div class="input-daterange input-group" id="bs_datepicker_range_container">
                            <div class="form-line">
                                <input name="checkin" type="text" class="form-control" placeholder="Date start...">
                            </div>
                            <span class="input-group-addon">to</span>
                            <div class="form-line">
                                <input name="checkout" type="text" class="form-control" placeholder="Date end...">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input name="nights" type="text" class="form-control" required>
                                <label class="form-label">Nights</label>
                            </div>
                            <div class="help-info">Min. 3, Max. 10 characters</div>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input name="PAX" type="number" class="form-control" required>
                                <label class="form-label">PAX</label>
                            </div>
                            <div class="help-info">How many guests</div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input name="pernight" type="text" class="form-control money-dollar" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Per Night</div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input name="cleanning" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Cleanning</div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input name="discountss" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Discount</div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input name="discount" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">Comissions</div>
                        </div>
                    </div>
                    {{-- <div class="col-xs-6"> --}}
                        <div class="input-group form-float">
                            <span class="input-group-addon">
                                <i class="material-icons">euro_symbol</i>
                            </span>
                            <div class="form-line">
                                <input name="receivable" type="text" class="form-control" placeholder="Ex: 99,99 €">
                            </div>
                            <div class="help-info">receivable</div>
                        </div>
                    {{-- </div> --}}
                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
            </form>
        </div>
    </div>
</div>
</div>



@endsection
