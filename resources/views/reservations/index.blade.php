@extends('layouts.app')

@section('content')


    <div class="container-fluid">
        <div class="block-header">
            <h2>
                RESERVATIONS
            </h2>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            RESERVATIONS
                            {{-- EXAMPLE --}}
                            {{-- <small>You can edit any columns except header/footer</small> --}}
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="customer/create">New Reservation</a></li>
                                    {{-- <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li> --}}
                                </ul>
                            </li>
                        </ul>
                    </div>
                    {{-- {{dd($customers)}} --}}
                    <div class="body table-responsive">
                        <table id="mainTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>CheckIn</th>
                                    <th>CheckOut</th>
                                    <th>Nights</th>
                                    <th>Payment</th>
                                    <th>Paied</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($customers as $customer)
                                <tr>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->checkin}}</td>
                                    <td>{{$customer->checkout}}</td>
                                    <td>{{$customer->nights}}</td>
                                    <td>{{$customer->receivable}}</td>
                                    @if ($customer->recieved == true)
                                    <td><i class="material-icons">monetization_on</i></td>
                                    @else
                                    <td><i class="material-icons">money_off</i></td>
                                    @endif
                                <td><a href="{{route('customer.edit', $customer->id)}}"><i class="material-icons">mode_edit</i>
                                    </a></td>
                                </tr>
                            </tbody>
                                @empty
                                <p>No users</p>

                                @endforelse

                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{$customer->sum('nights')}}</th>
                                    <th><strong>TOTAL {{$customer->sum('receivable')}}</strong></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
