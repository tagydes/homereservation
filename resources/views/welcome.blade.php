@extends('layouts.app')
<style>

</style>
@section('content')



<div class="container-fluid">
    <div class="block-header">
        <h2>DASHBOARD</h2>
    </div>
    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('customer.index')}}" style="text-decoration: none;" class=""> <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">RESERVATIONS</div>
                    <div class="number count-to" data-from="0" data-to="{{$reservations->count( )}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div></a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="content">
                    <div class="text">CURRENT MONTH</div>
                    {{-- {{dd($reservations->where('MONTH(checking)', ))}} --}}
                    <div class="number count-to" data-from="0" data-to="{{$currentM}}" data-speed="1000" data-fresh-interval="20"></div>

                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">monetization_on</i>
                </div>
                <div class="content">
                    <div class="text">INCOME</div>
                    <div class="number count-to" data-from="0" data-to="{{$income}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="content">
                    <div class="text">WAITTING</div>
                    <div class="number count-to" data-from="0" data-to="{{$waiting}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Widgets -->
</div>

<div class="container-fluid">
    <div class="row clearfix">
        @if( $checkout)
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="header bg-orange">
                    <h2>
                        Checking Out <small>Today is checking out</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <span class="pull-center"> <a href={{route('customer.edit',$checkout->id)}}><b> {{$checkout->name}}</b> </a></span>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if( $current)
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    Current Reservation <small>Current reservation </small>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <p> <i class="material-icons">person</i> <a href={{route('customer.edit',$current->id)}}><b> {{$current->name}}</b> </a> </p>
                <p> <i class="material-icons">group</i> PAX {{$current->pax}}</b> </p>
                <p> <i class="material-icons">date_range</i> Checkin Out <strong>{{$current->checkout}}</span></strong> </p>
            </div>
        </div>
    </div>
    @endif
    @if( $next)
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    Checking In <small>Next Checkin on</small>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <p> <i class="material-icons">person</i> <a href={{route('customer.edit',$next->id)}}><b> {{$next->name}}</b> </a> </p>
                <p> <i class="material-icons">group</i> PAX {{$next->pax}}</b> </p>
                <p> <i class="material-icons">date_range</i> Arriving  <strong>{{$next->checkin}}</span></strong> </p>
            </div>
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="body bg-blue">
                <div class="font-bold m-b--35">Income by platforms</div>
                <ul class="dashboard-stat-list">
                    <li>
                        Booking
                        <span class="pull-right"><b>{{number_format($recieved_booking, 2)}}€</b> <small>Earned</small></span>
                    </li>
                    <li>
                        Airbnb
                        <span class="pull-right"><b>{{number_format($recieved_airbnb, 2)}}€</b> <small>Earned</small></span>
                        <hr>
                    </li>
                    <li>
                        Total
                        <span class="pull-right"><b>{{number_format($recieved_airbnb+$recieved_booking ,2)}}€</b> <small>Earned</small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
        <div class="card">
            <div class="body bg-blue">
                <div class="font-bold m-b--35">Commissions paid by Platform</div>
                <ul class="dashboard-stat-list">
                    <li>
                        Booking
                        <span class="pull-right"><b>{{number_format($fees_booking ,2)}}€</b> <small></small></span>
                    </li>
                    <li>
                        Airbnb
                        <span class="pull-right"><b>{{number_format($fees_airbnb ,2)}}€</b> <small></small></span>
                        <hr>
                    </li>
                    <li>
                        Total
                        <span class="pull-right"><b>{{number_format($fees_airbnb+$fees_booking ,2)}}€</b> <small></small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #END# Answered Tickets -->
</div>
</div>

@endsection


