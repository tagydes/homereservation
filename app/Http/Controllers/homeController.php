<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Platform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class homeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $platform = Platform::get();

        $reservations = Customer::get();


        $income = collect($reservations)->where('recieved', true)->sum('receivable');
        $waiting = collect($reservations)->where('recieved', false)->sum('receivable');

        $recieved_booking = collect($reservations)->where('platform_id', 1)->sum('receivable');
        $recieved_airbnb = collect($reservations)->where('platform_id',2)->sum('receivable');

        $fees_booking = collect($reservations)->where('platform_id',1)->sum('comissions');
        $fees_airbnb = collect($reservations)->where('platform_id',2)->sum('comissions');

        $currentM = DB::table('customers')->whereMonth('checkin', now()->month)->count();

        $now = date('Y-m-d');
        // $now = "2020-08-25";
        // dd($now);
        $date = strtotime("+7 day");

        $today = Customer::where('checkin', $now)->first();

        $next = Customer::whereBetween('checkin', [$now, date('Y-m-d', $date)])->first();

        $before = strtotime("-3 day");

        // dd(date('Y-m-d', $before));
        $current = Customer::wherein('checkin', [date('Y-m-d', $before), $now]  )->first();
        $checkout = Customer::where('checkout', $now)->first();




        return view('welcome', compact('reservations', 'platform', 'today', 'next', 'current', 'income',
    'waiting', 'recieved_airbnb', 'recieved_booking', 'fees_booking', 'fees_airbnb', 'checkout', 'currentM'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function show(patform $patform)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function edit(patforms $patforms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, patforms $patforms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function destroy(patforms $patforms)
    {
        //
    }
}
