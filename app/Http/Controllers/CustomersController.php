<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Platform;
use App\customers;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $customers = Customer::get()->sortBy('checkin');
        // dd($customers);

        return view('reservations.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $platforms = Platform::get();

        return view('reservations.create', compact('platforms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Customer::Create([
            'platform_id' => $request->platform,
            'name'         => $request->guest_name,
            'checkin'      => date('Y-m-d', strtotime($request->checkin)),
            'checkout'     => date('Y-m-d', strtotime($request->checkout)),
            'nights'       => $request->nights,
            'pax'          => $request->PAX,
            'costs'        => $request->pernight,
            'comissions'   => $request->comissions,
            'cleanning'    => $request->cleanning,
            'receivable'    => $request->receivable,
            'recieved'    => '0',
            'documentation'    => '0',
            'discounts'     => $request->discounts
        ]);

        return view('customer');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $platforms = Platform::get();
        $customer = Customer::where('id',$id)->first();

        return view('reservations.edit', compact('customer', 'platforms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, customer $customer)
    {
        // dd($request->all());
    //     $this->validate($request, [
    //         'documentation' => 'required',
    //         'documentation.*' => 'mimes:doc,pdf,docx,zip'
    // ]);


    if($request->hasfile('documentation'))
     {
        foreach($request->file('documentation') as $file)
        {
            $name = time().'.'.$file->extension();
            $file->move(public_path().'/files/', $name);
            $data[] = $name;
        }
     }


    //  if ($request->has('recieved')) {
    //     //  dump($request->recieved);
    //     $recieved = $request->recieved;
    // }
// DD($request->recieved);
        $customer = Customer::find($customer->id);

        $customer->recieved = $request->recieved;
        $customer->platform_id = $request->platform;
        $customer->receivable = $request->receivable;
        $customer->comissions = $request->comissions;
        $customer->documentation = $request->documentation;

        $customer->save();
        // dd($customer->changes());
        // dd($customer);
        return back();


    //     Customer::Update([
    //         'platform_id' => $request->platform,
    //         'name'         => $request->guest_name,
    //         'checkin'      => date('Y-m-d', strtotime($request->checkin)),
    //         'checkout'     => date('Y-m-d', strtotime($request->checkout)),
    //         'nights'       => $request->nights,
    //         'pax'          => $request->PAX,
    //         'costs'        => $request->pernight,
    //         'comissions'   => '0',
    //         'cleanning'    => $request->cleanning,
    //         'receivable'    => '0',
    //         'recieved'    => '0',
    //         'documentation'    => '0',
    //         'discounts'     => $request->discount
    //         ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer $customers)
    {
        //
    }
}
