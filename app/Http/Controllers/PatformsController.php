<?php

namespace App\Http\Controllers;

use App\patforms;
use Illuminate\Http\Request;

class PatformsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function show(patforms $patforms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function edit(patforms $patforms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, patforms $patforms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\patforms  $patforms
     * @return \Illuminate\Http\Response
     */
    public function destroy(patforms $patforms)
    {
        //
    }
}
