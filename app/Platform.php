<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    public function customers()
	{
		return $this->belongsTo('App\Customer', 'id', 'platform_id');
    }


}
